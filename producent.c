//
//  main.c
//  SO
//
//  Created by Krystian on 17/06/2015.
//  Copyright (c) 2015 Krystian. All rights reserved.
//

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "semafor.h"

#define MAX 128
#define PRODUCENT 0
#define KONSUMENT 1
#define OUTPUT 2

struct buf_elem {
    long mtype;
    int mvalue;
} elem;

int blocker = 0;


int msgid;

static void signal_handler(int signo)
{
    switch (signo)
    {
        case SIGUSR1:
            elem.mtype = KONSUMENT;
            elem.mvalue = SIGUSR1;
                if (msgsnd(msgid, &elem, sizeof(elem.mvalue), 0) == -1){
                    perror("Wyslanie pustego komunikatu");
                    exit(1);
                }
            elem.mtype = OUTPUT;
            elem.mvalue = SIGUSR1;
            if (msgsnd(msgid, &elem, sizeof(elem.mvalue), 0) == -1){
                perror("Wyslanie pustego komunikatu");
                exit(1);
            }
            
            break;
        case SIGUSR2:
            if (msgrcv(msgid,&elem,sizeof(elem.mvalue),PRODUCENT,0) == -1){
                perror("Odebranie elementu");
                exit(1);
                }
            printf("Wartosc: %d\n", elem.mvalue);

            break;
        case SIGINT:
            break;
        case SIGCONT:
            printf("Proces %d wznowiony\n", getpid());
            blocker = 0;
            break;
            
    }
}
static void proc_signal(int signo)
{
    switch (signo) {
        case SIGUSR1:
            blocker = 1;
            while(blocker)
            {
                printf("Proces %d wstrzymany\n",getpid());
                sleep(1);
            }
            break;
            
        default:
            break;
    }
}
int main(){
    int shmid, semid,i;
    char* buf,c;
    
    
    
    msgid = msgget(45282, IPC_CREAT|IPC_EXCL|0600);
    if(msgid == -1)
    {
        msgid = msgget(45282, IPC_CREAT | 0600);
        if(msgid == -1)
        {
            perror("Utworzenie kolejki komunikatow");
            exit(1);
        }
    }
    
    shmid = shmget(45282, (MAX+2)*sizeof(char), IPC_CREAT|0600);
    if (shmid == -1){
        perror("Utworzenie segmentu pamieci wspoldzielonej");
        exit(1);
    }
    buf = (char*)shmat(shmid, NULL, 0);
    if (buf == NULL){
        perror("Przylaczenie segmentu pamieci wspoldzielonej");
        exit(1);
    }
    #define indexZ buf[MAX]
    #define indexO buf[MAX+1]
    semid = semget(45282, 4, IPC_CREAT|IPC_EXCL|0600);
    if (semid == -1){
        semid = semget(45282, 4, 0600);
        if (semid == -1){
            perror("Utworzenie tablicy semaforow");
            exit(1);
            }
    }
    else{
        indexZ = 0;
        indexO = 0;
        if (semctl(semid, 0, SETVAL, (int)MAX) == -1){
            perror("Nadanie wartosci semaforowi 0");
            exit(1);
        }
        if (semctl(semid, 1, SETVAL, (int)0) == -1){
            perror("Nadanie wartosci semaforowi 1");
            exit(1);
            }
        if (semctl(semid, 2, SETVAL, (int)MAX) == -1){
            perror("Nadanie wartosci semaforowi 2");
            exit(1);
        }
        if (semctl(semid, 3, SETVAL, (int)0) == -1){
            perror("Nadanie wartosci semaforowi 3");
            exit(1);
        }

        }
    
    while( (c=getchar()) != EOF)
    {
        opusc(semid, 0);
        if(c == '\n') continue;
        buf[indexZ] = c;
        indexZ = (indexZ+1)%MAX;
        podnies(semid, 1);
        
        fflush(stdin);
    }
}