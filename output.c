//
//  output.c
//  SO
//
//  Created by Krystian on 17/06/2015.
//  Copyright (c) 2015 Krystian. All rights reserved.
//

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdlib.h>
#include <stdio.h>
#include "semafor.h"

#define MAX 128
int main(){
    int shmid, shmid2, semid, i;
    char *buf;
    char *bufSzyfr;
    shmid = shmget(45282, (MAX+2)*sizeof(char), IPC_CREAT|0600);
    if (shmid == -1){
        perror("Utworzenie segmentu pamieci wspoldzielonej");
        exit(1);
    }
    shmid2 = shmget(45281, (MAX + 2)*sizeof(char), IPC_CREAT|0600);
    if (shmid2 == -1){
        perror("Utworzenie segmentu pamieci wspoldzielonej");
        exit(1);
    }
    buf = (char*)shmat(shmid, NULL, 0);
    if (buf == NULL){
        perror("Przylaczenie segmentu pamieci wspoldzielonej");
        exit(1);
    }
    bufSzyfr = (char*)shmat(shmid2, NULL, 0);
    if (bufSzyfr == NULL){
        perror("Przylaczenie segmentu pamieci wspoldzielonej");
        exit(1);
    }
    
#define indexZ buf[MAX]
#define indexO bufSzyfr[MAX+1]
    
    semid = semget(45282, 4, IPC_CREAT|IPC_EXCL|0600);
    if (semid == -1){
        semid = semget(45282, 4, 0600);
        if (semid == -1){
            perror("Utworzenie tablicy semaforow");
            exit(1);
        }
    }
    else{
        indexZ = 0;
        indexO = 0;
        if (semctl(semid, 0, SETVAL, (int)MAX) == -1){
            perror("Nadanie wartosci semaforowi 0");
            exit(1);
        }
        if (semctl(semid, 1, SETVAL, (int)0) == -1){
            perror("Nadanie wartosci semaforowi 1");
            exit(1);
        }
        if (semctl(semid, 2, SETVAL, (int)MAX) == -1){
            perror("Nadanie wartosci semaforowi 2");
            exit(1);
        }
        if (semctl(semid, 3, SETVAL, (int)0) == -1){
            perror("Nadanie wartosci semaforowi 3");
            exit(1);
        }

    }
    
    for (i=0; i<10000; i++){
        opusc(semid, 3);
        printf("Numer: %d Wartosc: %c\n", i, bufSzyfr[indexO]);
        indexO = (indexO+1)%MAX;
        podnies(semid, 2);
    }
}